print("===Calculadora===")   # Escribir calculadora
print("Elige una opcion")    # Para que sepan que tienen que escoger una opcion
print("1-Suma")              # La opcion de sumar
print("2-Resta")             # La opcion de rescar
print("3-Multiplicación")    # La opcion de multiplicar
print("4-División")          # La opcion de dividir
x=input("Escribe tu elección:")  # input: Inroduir datos de distintos tipos
y=float(x)    # Escoge 1,2,3 o 4

if(y==1):        # Si escriben en la terminal el numero uno sera una suma
    a=input("Introduce el primer numero\n")    # Escribe el primer numero para calcular  # \n Es para escribir el numero
    b=input("Introduce el segundo numero\n")   # Escribe el segundo numero para calcular
    a2=float(a)      # float (a) guarda el numero escogido en el comando a=input, puede guardar numeros con decimales
    b2=float(b)      # float (b) guarda el comando b=input
    resultado= a2+b2
    print("El resultado de la suma es:",resultado) # Que en la pantalla aparezca el mensaje el resultado de la suma es: con el resultado
elif(y==2):    # elif: Es if pero se usa cuando hay muchas opciones
    a=input("Introduce el primer numero\n")
    b=input("Introduce el segundo numero\n")
    a2=float(a)
    b2=float(b)
    resultado=a2-b2
    print("El resultado de la resta es:",resultado)
elif(y==3):
    a=input("Introduce el primer numero\n")
    b=input("Introduce el segundo numero\n")
    a2=float(a)
    b2=float(b)
    resultado=a2*b2
    print("El reultado de la multiplicación es",resultado)
elif(y==4):
    a=input("Introduce el primer numero\n")
    b=input("Introduce el segundo numero\n")
    a2=float(a)
    b2=float(b)
    resultado=a2/b2
    print("El resultado de la división es",resultado)
else:   # Else es sino se puede ninguna condicion
    print("NUMERO INVALIDO")  
