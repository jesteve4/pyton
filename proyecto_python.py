import turtle

Jugador1= 0
Jugador2= 0

wn = turtle.Screen()
wn.title("Pong per Joan Esteve")
wn.bgcolor("black")
wn.setup(width=1000, height=600)

#Pala A
pala_a= turtle.Turtle()
pala_a.speed(0)
pala_a.shape("square")
pala_a.color("white")
pala_a.shapesize(stretch_wid=5, stretch_len=1)
pala_a.penup()
pala_a.goto(-350, 0)
#Pala B
pala_b= turtle.Turtle()
pala_b.speed(0)
pala_b.shape("square")
pala_b.color("white")
pala_b.shapesize(stretch_wid=5, stretch_len=1)
pala_b.penup()
pala_b.goto(350, 0)

#Bola
bola = turtle.Turtle() 
bola.speed(30) 
bola.shape("circle") 
bola.color("white") 
bola.penup() 
bola.goto(0, 0) 
bola.x = 5
bola.y =-5

#Puntos

sketch = turtle.Turtle() 
sketch.speed(0) 
sketch.color("white") 
sketch.penup() 
sketch.hideturtle() 
sketch.goto(0, 260) 
sketch.write("Jugador1 : 0 Jugador2: 0", 
			align="center", font=("Comic Sans", 25, "normal"))
#Funciones
#Pala A
def pala_a_up():
	y = pala_a.ycor()
	y += 20
	pala_a.sety(y)
def pala_a_down():
	y = pala_a.ycor()
	y -= 20
	pala_a.sety(y)
#Pala B
def pala_b_up():
	y = pala_b.ycor()
	y += 20
	pala_b.sety(y)
def pala_b_down():
	y = pala_b.ycor()
	y -= 20
	pala_b.sety(y)
#Teclas
wn.listen()
#Pala A
wn.onkeypress(pala_a_up, "w")
wn.onkeypress(pala_a_down, "s")
#Pala B
wn.onkeypress(pala_b_up, "Up")
wn.onkeypress(pala_b_down, "Down")
#Juego
while True:
	wn.update()

	# Movimiento de la bola
	bola.setx(bola.xcor() + bola.x)
	bola.sety(bola.ycor() + bola.y)
	#Borde
	if bola.ycor() >600:
		bola.sety(100)
		bola.y *=-1

	if bola.ycor() >-600:
		bola.sety(-10)
		bola.y *=-1

	if bola.xcor() > 500:
		bola.goto(250, 250)
		bola.x *=-1
		Jugador1 += 1
		sketch.write("Jugador1 : {} Jugador2: {}".format( 
					Jugador1, Jugador2), align="center", 
					font=("Comic Sans", 25, "normal"))

	if  bola.xcor() < -500:
		bola.goto(250, 250)
		bola.x *=-1
		Jugador2 += 1
		sketch.write("Jugador1 : {} Jugador2: {}".format( 
					Jugador1, Jugador2), align="center", 
					font=("Comic Sans", 25, "normal"))

	#Borde Palas
	if (bola.xcor() > 360 and
       bola.xcor() < 370) and (bola.ycor() < pala_b.ycor()+40 and bola.ycor() > pala_b.ycor()-40):
		bola.setx(360) 
		bola.x*=-1
	if (bola.xcor()<-360 and bola.xcor()>-370) and (bola.ycor()<pala_a.ycor()+40 and bola.ycor()>pala_a.ycor()-40):                                      
			bola.setx(-360) 
			bola.x*=-1